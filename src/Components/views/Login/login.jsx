
import React, { Component } from 'react';
import { Row, Col,Form, Input, Button, Checkbox, notification } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import { save_user } from '../../functions/check_user';
import { language } from '../../functions/language';
import  Logins  from '../../../styles/Login';
import {
    Redirect
  } from "react-router-dom";

export default class Login extends Component {
    state = {
        loadings: false,
        log: this.props.check_use,
    }
    onFinish = async (values) => {
        this.setState({
            loadings: true
        })

        let log = await save_user(values);
        if (!log) {
            notification.error({
                message: language().login.alertError.title,
                description:
                    language().login.alertError.caption
              })
        }

        this.setState({
            log,
            loadings: false
        })
    };
    
    render() {

        let { log,loadings } = this.state;
        let { isMobile } = this.props;
        if( log ) {
            return <Redirect to="/vehicles"/>
        } 

        const layout = {
        labelCol: { span: 24 },
        wrapperCol: { span: 24 },
        };
        const tailLayout = {
        wrapperCol: { span: 24 },
        };
        return(
            <Logins>
                
                <Row className="gutter-row">
                    { !isMobile &&
                        <Col sm={15} id="left">
                            {/* <img src="https://crm7.com.br/wp-content/uploads/2020/03/6-maneiras-pr%C3%A1ticas-de-melhorar-as-vendas-automatizando-seus-fluxos-de-trabalho-de-CRM.jpg" alt=""/> */}
                            <img src="https://app.girocar.com.br/assets/img/logo.png?v=1617332083" alt="" className="img"/>
                        </Col>
                    }
                    <Col sm={9} xs={24}>

                        <div className="card">
                            <main className="body">
                                <h1>{language().login.title}</h1>
                                <h3>{language().login.caption}</h3>
                                <Form
                                    {...layout}
                                    name="login"
                                    initialValues={{ remember: true }}
                                    onFinish={this.onFinish}
                                    >
                                    <Form.Item
                                        name="username"
                                        rules={[{ required: true, message: language().login.alertUser }]}
                                    >
                                        <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder={language().login.inputUser}/>
                                    </Form.Item>

                                    <Form.Item
                                        name="password"
                                        rules={[{ required: true, message: language().login.alertPassword }]}
                                    >
                                        <Input.Password prefix={<LockOutlined className="site-form-item-icon" />} placeholder={language().login.inputPassword} />
                                    </Form.Item>

                                    <Form.Item {...tailLayout} name="remember" valuePropName="checked">
                                        <Checkbox>{language().login.logged}</Checkbox>
                                    </Form.Item>

                                    <Form.Item {...tailLayout}>
                                        <Button type="primary" htmlType="submit" loading={loadings}>
                                            {language().login.button}
                                        </Button>
                                    </Form.Item>
                                </Form>
                                <Col span={19} id="end-body">
                                    <hr/>
                                    <p>{language().login.help}</p>
                                    <div className="links">
                                        <a href="#id">(51) 3061-6037</a>
                                        <a href="#id">atendimento@leadforce.com.br</a>
                                    </div>
                                </Col>
                            </main>
                            <footer>
                                <img src="https://app.girocar.com.br/assets/img/logo-lead.png?v=1617332083" alt=""/>
                                <span>© 2021 Lead Force. {language().login.direct}</span>
                            </footer>
                        </div>
                    </Col>
                </Row>
            </Logins>
        )
    }
}