
import React from 'react';
import { List, Avatar, Space,Empty } from 'antd';
import Icon from '@mdi/react';
import { mdiCarWindshieldOutline,mdiCarElectricOutline,mdiCarCruiseControl,mdiCarClutch } from '@mdi/js';
const listData = [];
for (let i = 0; i < 23; i++) {
  listData.push({
    key:i,
    href: 'https://ant.design',
    title: `Renault Logan`,
    avatar: 'https://http2.mlstatic.com/D_NQ_NP_607478-MLB27450551369_052018-O.jpg',
    description:
      'A vida em modo conforto.',
    // content:
    //   'We supply a series of design principles, practical patterns and high quality design resources (Sketch and Axure), to help people create their product prototypes beautifully and efficiently.',
  });
}

const IconText = ({ icon, text }) => (
  <Space style={{display: 'flex',alignItems: 'center'}}>
    <Icon path={icon} color="rgba(0, 0, 0, 0.45)" size={1.3} style={{paddingTop: '10px'}}/>
    <b>{text}</b>
  </Space>
);

export default function list_vehicles({listVehicles,filterPage}){
    
    return(
        <List
            itemLayout="vertical"
            size="large"
            pagination={{
            onChange: page => {
              filterPage(page);
            },
            current: listVehicles.current_page,
            showLessItems: false,
            showSizeChanger: false,
            total: listVehicles.total || 0,
            showTotal: false,
            pageSize: listVehicles.per_page || 0,
            }}
            locale={
              {emptyText: <Empty
                image="https://gw.alipayobjects.com/zos/antfincdn/ZHrcdLPrvN/empty.svg"
                imageStyle={{
                  height: 60,
                }}
                description={
                  <span>
                    NÃO HÁ VEÍCULOS NA SUA LISTA
                  </span>
                }
              >
              </Empty>
            }}
            dataSource={listVehicles.data}
            renderItem={item => (
            <List.Item
                key={item.id}
                actions={[
                <IconText icon={mdiCarWindshieldOutline} text={`Placa: ${item.board}`} key="list-vertical-star-o" />,
                <IconText icon={mdiCarElectricOutline} text={`Marca: ${item.brand}`} key="list-vertical-like-o" />,
                <IconText icon={mdiCarCruiseControl} text={`Modelo: ${item.model}`} key="list-vertical-message" />,
                <IconText icon={mdiCarClutch} text={`Versão: ${item.version}`} key="list-vertical-message" />,
                ]}
                extra={
                <img
                    width={272}
                    alt="logo"
                    src={item.image}
                />
                }
            >
                <List.Item.Meta
                avatar={<Avatar src="https://http2.mlstatic.com/D_NQ_NP_607478-MLB27450551369_052018-O.jpg" />}
                title={<a href={item.href}>{item.version}</a>}
                description={`Portais publicados: ${String(item.published_portals.map( portais => portais.name))}`}
                />
                {item.content}
            </List.Item>
            )}
        />
    ) 
}