

import React, { Component } from "react";
import { Card, Col, Row } from 'antd';
import { mdiFlagVariantOutline } from '@mdi/js';
import Icon from '@mdi/react';
// import { dealers_list } from "../../functions/dealers";

export default class Index extends Component {
    
    render() {
        let { vehicles,vehicles_publisheds,vehicles_updates,dealers,vehicles_erros } = this.props;
        return(
            <Row gutter={16}>
                <Col span={6} md={6} xs={24} className="mt-1" >
                    <Card title="Meu estoque" bordered={false} className="h-100 card-custom c-whrite b-default text-center">
                        <span>Você tem cadastrados</span>
                        <strong className="font-30 w-100 d-block">{vehicles.total} veículos</strong>
                        <div className="options d-flex align-items-center mt-2">
                            <span className="anuncios w-100">
                                <p className="p-0">Anunciados</p>
                                <p className="font-25"><strong>{vehicles_publisheds.portals_published || 0}</strong></p>
                            </span>
                            <hr/>
                            <span className="add-vehicles w-100">
                                <p className="p-0">Adicionar veículo</p>
                                <p><Icon path={mdiFlagVariantOutline} size={2}/></p>
                            </span>
                        </div>
                    </Card>
                </Col>
                <Col span={6} md={6} xs={24} className="mt-1">
                    <Card title="Veículos últimos 30 Dias" bordered={false} className="h-100 card-custom c-whrite b-default2 text-center">
                        <div style={{padding: '15%'}}>
                            <span>Você tem</span>
                            <strong className="font-30 w-100 d-block">{vehicles_updates.total} Novos</strong>
                        </div>
                        {/* <div className="options d-flex align-items-cente mt-2">
                            <span className="anuncios w-100">
                                <p className="p-0">Não respondidos</p>
                                <p className="font-25"><strong>15</strong></p>
                            </span>
                            <hr/>
                            <span className="add-vehicles w-100">
                                <p className="p-0">Vendidos</p>
                                <p className="font-25"><strong>30</strong></p>
                            </span>
                        </div> */}
                    </Card>
                </Col>
                <Col span={6} md={6} xs={24} className="mt-1">
                    <Card title="Portais" bordered={false} className="card-custom text-center">
                        <span>Você têm ativos</span>
                        <strong className="font-30 w-100 d-block">{dealers.length} Portais</strong>
                    </Card>
                    <Card title="Resumo" className="mt-1 card-custom" bordered={false}>
                        <span className="c-orange d-flex justify-content-between"><strong>Anúncios integrados</strong><strong>{vehicles_publisheds.portals_published || 0}</strong></span>
                        <span className="c-red d-flex justify-content-between mt-1"><strong>Anúncios error</strong> <strong>{vehicles_erros.portals_error || 0}</strong></span>
                    </Card>
                </Col>
                <Col span={6} md={6} xs={24} className="mt-1">
                    <Card title="Comprar no Repasse" bordered={false} className="card-custom text-center">
                        <span>Lojas parceiras</span>
                        <strong className="font-30 w-100 d-block">05</strong>
                    </Card>
                    <Card title="Consultar historico veículos" className="mt-1 card-custom" bordered={false}>
                        <span className="d-flex justify-content-between"><strong>Auto Crivo</strong><button className="btn"><strong>Acessar</strong></button></span>
                        <span className="d-flex justify-content-between mt-1"><strong>Dossiê Total</strong><button className="btn"><strong>Acessar</strong></button></span>
                    </Card>
                </Col>
            </Row>
        )
    }
}