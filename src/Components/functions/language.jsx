

const languageJson = require(`./languageJson.json`);
export function language() {
    const language = navigator.language;

    switch(language) {
        case "en-US":
            return(
                languageJson
            )
        default:
            return(
                {
                    login: {
                        alertUser: "Por favor insira seu usuário",
                        alertPassword: "Por favor insira sua senha",
                        title: "Seja bem vindo",
                        caption: "Para continuar insira seu usuário e senha",
                        direct: "Todos os direitos reservados.",
                        logged: "Permanecer Logado",
                        inputUser: "Usuário",
                        inputPassword: "Senha",
                        help: "Precisa de ajuda ?",
                        button: "Entrar",
                        alertError: {
                            title: "Error login",
                            caption: "Senha ou usuário incorretos"
                        }
                    }
                }
            )
    }
}