import React, { Component } from 'react';

export default class Teste extends Component {
    state = {
        inputs: [
            {
                name: "teste",
                placeholder: "teste",
                type: "input",
                value: ""
            },
            {
                name: "teste",
                placeholder: "teste",
                type: "select",
                options: [

                ]
            }
        ]
    }
    render() {
        let { inputs } = this.props;
        return(
            <div>
                {
                    inputs.map( typeForm => {
                        if (typeForm.type === "input")
                        return(
                            <input name={typeForm.name} placeholder={typeForm.placeholder} onChange={(value) => this.function(typeForm.value)} />
                        )
                    })
                }
            </div>
        )
    }
}
