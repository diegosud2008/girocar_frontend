
import React, { Component } from 'react';
import { Row, Col,Form, Input, Select,Button,Checkbox } from 'antd';
import { DadoQuery,executeQuery,InsertVehicle } from '../../../functions/query';
import { ModalQuery } from './modalQuery';

const { Option } = Select;

const layout = {
    labelCol: { span: 24 },
    wrapperCol: { span: 24 },
  };

export default class Interna extends Component{
    state = {
        dados: {},
        load: false,
        isModalVisible: false,
        dadosRequest: {veiculo: []},
        importVehicles: false,
        loadButton: false
    }
    async componentDidMount() {
        const id = this.props.match.params.id;
        this.setState({
            load: true
        })
        let dados = await DadoQuery(id,this.props.companie) || {};
        console.log(dados)
        this.setState({
            load: false,
            dados
        })
    }
    onFinish = async (value) => {
        const id = this.props.match.params.id;
        this.setState({
            loadButton: !this.state.loadButton
        })
        let dadosRequest = await executeQuery(this.props.companie,{...value,add: id});
        if (this.state.importVehicles) {
            InsertVehicle(dadosRequest.veiculo,this.props.companie)
        }
        this.setState({
            isModalVisible: true,
            loadButton: !this.state.loadButton,
            dadosRequest
        })
    }
    handleOk = () => {
        this.setState({
            isModalVisible: false,
        })
    };
    
    handleCancel = () => {
        this.setState({
            isModalVisible: false,
        })
    };
    onChange = () => {
        this.setState({
            importVehicles: !this.state.importVehicles
        })
    }
    render(){
        let { dados,load,dadosRequest,loadButton,isModalVisible,importVehicles } = this.state;

        return(
            <>
            {
            load ?
            null
            :
            <>
                <Form name="basic" {...layout} onFinish={this.onFinish} >
                    <Row>
                        <Col xs={{ span: 5, offset: 1 }} lg={{ span: 6 }}>
                            <Form.Item
                                label="Título"
                                name="title"
                                initialValue={dados.title}
                            >
                                <Input className="w-100"/>
                            </Form.Item>
                            <Form.Item
                                label="Tipo"
                                name="type"
                                initialValue={dados.type}
                            >
                                <Select>
                                    <Option value="Mysql">Mysql</Option>
                                    <Option value="SQL Server">SQL Server</Option>
                                    <Option value="DealerUp/NBS">DealerUp/NBS</Option>
                                </Select>
                            </Form.Item>
                        </Col>
                        <Col xs={{ span: 11, offset: 1 }} lg={{ span: 6 }}>
                            <Form.Item
                                    label="Username"
                                    name="username"
                                    initialValue={dados.username}
                                >
                                <Input className="w-100"/>
                            </Form.Item>
                            <Form.Item
                                label="Senha"
                                name="password"
                                initialValue={dados.password}
                            >
                                <Input className="w-100"/>
                            </Form.Item>
                        </Col>
                        <Col xs={{ span: 5, offset: 1 }} lg={{ span: 6 }}>
                            <Form.Item
                                label="Host"
                                name="host"
                                initialValue={dados.host}
                            >
                                <Input className="w-100"/>
                            </Form.Item>
                            <Form.Item
                                label="Banco de dados"
                                name="database"
                                initialValue={dados.database}
                            >
                                <Input className="w-100"/>
                            </Form.Item>
                        </Col>
                        <Col xs={{ span: 20, offset: 1 }} lg={{ span: 20 }}>
                            <Form.Item
                                label="SQL"
                                name="sql"
                                initialValue={dados.sql}
                            >
                                <Input.TextArea rows={10}></Input.TextArea>
                            </Form.Item>
                            <Button className="mr-2" type="primary" htmlType="submit" loading={loadButton}>Executar SQL</Button>
                            <Checkbox checked={importVehicles} onChange={this.onChange}>Importar veículos</Checkbox>
                        </Col>
                    </Row>
                </Form>
                {
                isModalVisible &&
                <ModalQuery isModalVisible={isModalVisible} dadosRequest={dadosRequest} title={dados.title} handleOk={this.handleOk} handleCancel={this.handleCancel} />
                }
            </>
            }
            </>
        )
    }
}