
import React, { Component } from 'react';
import Listagem from './listagem';
import { Query } from '../../functions/query';

export default class Index extends Component{
    state = {
        querys: []
    }
    async componentDidMount(){
        this.setState({
            querys: await Query(this.props.companie)
        })
    }
    async componentDidUpdate(prevProps) {
        if (prevProps.companie !== this.props.companie) {
            this.setState({
                querys: await Query(this.props.companie)
            })  
        }
    }
    render(){
        return(
            <section>
                <Listagem querys={this.state.querys} companie={this.props.companie} />
            </section>
        )
    }
}