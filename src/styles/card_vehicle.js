import styled from 'styled-components';

const Wrapper = styled.div`
.img {
    width: 28%;
    max-height: 260px;

    img {
        height: 100%;
        object-fit: cover;
    }
}
.infos {
    margin-left: 30px;
    width: 57%;
    font-weight: 300;
    span.title {
        font-size: 30px;
        line-height: 0;
    }
    hr {
        margin-top: 20px;
    }
    .info {
        font-size: 22px;
        line-height: 41px;
    }
}
span.shares {
    width: 14%;
    padding-left: 35px;
    border-left: 1px solid;
    height: 200px;
    display: flex;
    flex-wrap: wrap;
    align-items: center;
    font-size: 19px;
    line-height: 2;
    cursor: pointer;
    
    strong {
        margin-left: 7px;
    }
}
`

export default Wrapper;