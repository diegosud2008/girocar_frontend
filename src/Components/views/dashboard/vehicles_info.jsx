
import React, { Component } from 'react';
import { Col, Row, Card, Pagination, Popover} from 'antd';
import CardVehicle from '../vehicles/card_vehicle';

export default class VehiclesInfo extends Component {
    content = (dado) => {
        return (
            <div>
                <p className="text-center"><b>{dado.status === 'published' ? 'Publicado' : dado.status === 'sending' ? 'Processando' : dado.status}</b></p>
                {
                dado.status === 'error' &&
                    <p className="text-center"><b>{dado.return}</b></p>
                }
            </div>
        )
    }
    render() {
        let { vehicles,handlePage } = this.props;
        return(
            <Row gutter={15}>
                <Col span={18} md={18} xs={24} className="col-portal" >
                    {vehicles && vehicles.data.map( vehicle => {
                        return(
                            <Card className="card-custom mt-1" key={vehicle.id}>
                                <CardVehicle {...vehicle} />
                            </Card>
                        )
                    })}
                </Col>
                <Col span={6} md={6} xs={24} className="col-portal">
                {
                vehicles &&
                    vehicles.data.map( (vehicle,index) => {
                        return(
                            <Card title="Status dos seus anúncios" className="card-custom d-block text-center mt-1" key={index}>
                                <div className="portais">
                                    {
                                    vehicle.published_portals.map( (portal,index) => {
                                            return (
                                                <Popover title={() => (<div className="text-center">{portal.name}</div>)} content={() => this.content(portal)}>
                                                    <button className={`btn ${portal.status}`} key={index}><strong>{portal.name.toLocaleLowerCase().includes('webmotors') ? 'Webmotos' : portal.name}</strong></button>
                                                </Popover>
                                            )
                                        })
                                    }
                                </div>
                            </Card>
                        )
                    })
                }
                </Col>
                {
                vehicles &&
                    <Col span={24} md={24} xs={24} className="mt-1">
                        <Pagination defaultCurrent={1} total={vehicles.total} onChange={handlePage} /> 
                    </Col>
                }
            </Row>
        )
    }
}