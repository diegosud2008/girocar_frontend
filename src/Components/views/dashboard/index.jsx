
import React, { Component } from "react";
import Infos from "./sistem_info";
import VehiclesInfo from './vehicles_info';
import Style from '../../../styles/dashboard';
import { vehicles,show_vehicles_publisheds,filter_vehicles } from '../../functions/vehicles';
import { dealers_list } from '../../functions/dealers';

export default class Index extends Component {
    state = {
        loading: false,
        vehicles: {},
        vehicles_updates: {},
        vehicles_publisheds: {},
        vehicles_erros: {},
        dealers: [],
        page: 1
    }
    handlePage = async (page) => {
        let listvehicles = await vehicles(this.props.companie,page);
        this.setState({
            vehicles: listvehicles
        })
    }
    async componentDidMount () {
        this.setState({
            loading: true
        })
        let paramrs = [
            {
                WHERE: true
            },
            {
                UPDATED: true
            }
        ]
        

        let listvehicles = await vehicles(this.props.companie);
        let vehicles_publisheds = await show_vehicles_publisheds(this.props.companie,'published');
        let vehicles_erros = await show_vehicles_publisheds(this.props.companie,'error');
        let vehicles_updates = await filter_vehicles(paramrs,this.props.companie);
        let dealers = await dealers_list(this.props.companie);
        this.setState({
            vehicles: listvehicles,
            vehicles_publisheds,
            vehicles_erros,
            vehicles_updates,
            dealers,
            loading: false
        })
    }
    async componentDidUpdate(prevProps) {
        if (prevProps.companie !== this.props.companie) {
            this.setState({
                loading: true
            })
            let paramrs = [
                {
                    WHERE: true
                },
                {
                    UPDATED: true
                }
            ]
            let listvehicles = await vehicles(this.props.companie);
            let vehicles_publisheds = await show_vehicles_publisheds(this.props.companie,'published');
            let vehicles_erros = await show_vehicles_publisheds(this.props.companie,'error');
            let vehicles_updates = await filter_vehicles(paramrs,this.props.companie);
            let dealers = await dealers_list(this.props.companie);
            this.setState({
                vehicles: listvehicles,
                vehicles_publisheds,
                vehicles_erros,
                vehicles_updates,
                dealers,
                loading: false
            })
        }
    }
    render() {
        
        return(
            <Style>
                <div className="site-card-wrapper">
                    <Infos {...this.state} />
                    {
                        this.state.vehicles.data &&
                        <VehiclesInfo {...this.state} handlePage={this.handlePage} />
                    }
                </div>
            </Style>
        )
    }
}