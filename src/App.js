
import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import Login from './Components/views/Login/login';
import Vehicles from './Components/views/vehicles/vehicles';
import Dashboard from './Components/views/dashboard/index';
import Query from './Components/views/query/index';
import QueryInterna from './Components/views/query/interna/index';
import { PrivateRoute } from './autMobile';
import { check_use } from './Components/functions/check_user';
import { set_companie } from './Components/functions/search_companies';


let Companie = set_companie();
  

  const Routes = () => {
    function detectar_mobile(props) { 
      if( navigator.userAgent.match(/Android/i)
      || navigator.userAgent.match(/webOS/i)
      || navigator.userAgent.match(/iPhone/i)
      || navigator.userAgent.match(/iPad/i)
      || navigator.userAgent.match(/iPod/i)
      || navigator.userAgent.match(/BlackBerry/i)
      || navigator.userAgent.match(/Windows Phone/i)
      ){
        return true;
      } else {
        return false;
      }
    }
    function onChange(company) {
      console.log(company)
      Companie = company
    }
    return (
      <Router>
        <Switch>
          <Route path="/login" component={() => <Login check_use={check_use()} isMobile={detectar_mobile()} />} />
          <PrivateRoute path="/query" onChange={onChange} component={() => <Query companie={Companie}/>} />
          <PrivateRoute exact path="/QueryInterna/:id" onChange={onChange} component={props => <QueryInterna {...props} companie={Companie}/>} />
          <PrivateRoute path="/vehicles" onChange={onChange} component={() => <Vehicles companie={Companie}/>} />
          <PrivateRoute path="/dashboard" onChange={onChange} component={() => <Dashboard companie={Companie}/>} />
          <PrivateRoute path="/" onChange={onChange} component={() => <Dashboard companie={Companie}/>} />
        </Switch>
    </Router>
  )}


export default Routes;
