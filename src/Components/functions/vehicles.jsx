
export async function vehicles(company = "",page = "") {
    company = company || localStorage.setCompany;
    let vehicles = await fetch(`http://127.0.0.1:8000/api/vehicles/${company}?page=${page}`).then(dados => dados.json());
    return vehicles;
}

/**
 * 
 * @param {*} company id da empresa
 * @param {error,published,process} parames os dados passados pelo parametros deve ser o retorno do dado no banco para poder filtrar
 * @returns 
 */

export async function show_vehicles_publisheds(company = "",parames = "") {
    company = company || localStorage.setCompany;
    let vehicles = await fetch(`http://127.0.0.1:8000/api/show_vehicles_publisheds/${company}?published=${parames}`).then(dados => dados.json());
    return vehicles;
}
/**
 * 
 * @param {*} params parametros para filtros personalizados e passar para a API via post
 * @param {*} company id da empresa
 * @returns 
 */
export async function filter_vehicles(params = [],company) {
    var formdata = new FormData();    
    
    params.map( (value) => {
        formdata.append(String(Object.keys(value)), value[String(Object.keys(value))]);
        return true;
    })

    let vehicles = await fetch(`http://127.0.0.1:8000/api/vehicles/${company}`, {
        method: 'POST',
        body: formdata
    }).then(dado => dado.json())
    return vehicles;
}

/**
 * 
 * @param {*} category_id id da categoria
 * @returns 
 */

export async function vehicles_brands(category_id = 1) {
    let brands = await fetch(`http://127.0.0.1:8000/api/brands/${category_id}`).then(dados => dados.json());

    return brands;
}

/**
 * 
 * @param {*} models_id id do modelo
 * @returns 
 */
export async function vehicles_models(models_id = 0) {
    let models = await fetch(`http://127.0.0.1:8000/api/models/${models_id}`).then(dados => dados.json());

    return models;
}

/**
 * 
 * @param {*} vehicles_versios_id id da versão passada
 * @param {*} year_model ano final escolhido no select
 * @returns 
 */
export async function vehicle_versions(vehicles_versios_id = 0,year_model = 0) {
    let versions = await fetch(`http://127.0.0.1:8000/api/versions/${vehicles_versios_id}/${year_model}`).then(dados => dados.json());

    return versions
}

/**
 * 
 * @returns combustiveis cadastrados no banco
 */

export async function Vehicles_fuels() {
    let fuels = await fetch(`http://127.0.0.1:8000/api/fuels`).then(dados => dados.json());

    return fuels;
}

/**
 * 
 * @returns as cores cadastrados no banco
 */

export async function Vehicles_colors() {
    let colors = await fetch(`http://127.0.0.1:8000/api/colors`).then(dados => dados.json());

    return colors;
}

/**
 * 
 * @returns as seguimentos cadastrados no banco
 */

 export async function Vehicles_segments() {
    let segments = await fetch(`http://127.0.0.1:8000/api/segments`).then(dados => dados.json());

    return segments;
}

/**
 * 
 * @returns as cambios cadastrados no banco
 */

 export async function Vehicles_exchanges() {
    let exchanges = await fetch(`http://127.0.0.1:8000/api/exchanges`).then(dados => dados.json());

    return exchanges;
}


/**
 * 
 * @param {*} paramers dados do veiculo para serem salvo
 */
export async function Vehicle_save(paramers = {}) {
    if (paramers.id) {
        var formdata = new FormData();
        Object.entries(paramers).map( paramer => {
            if (paramer[0] !== 'vehicle_category_id' && paramer[0] !== 'vehicle_model_id' && paramer[0] !== 'vehicle_brand_id') {

                if (paramer[0] === 'status') {
                    formdata.append(paramer[0],paramer[1] === 0 ? 0 : 1);
                } else {
                    formdata.append(paramer[0],paramer[1] || "");
                }
            }
            return true;
        })
        return await fetch(`http://127.0.0.1:8000/api/vehicles_pull/${paramers.id}`,{
            method: 'POST',
            body: formdata
        });
    }
}

export async function Vehicle_save_img(imgs = {},id = '',placa = '',vehicle_id, type, ordem) {
    if (id) {
        var formdata = new FormData();
        formdata.append(type,imgs);
        formdata.append('PLACA',placa);
        formdata.append('TOKEN',id);
        let imagem = await fetch(`http://girocar.leadforce.com.br/ws/girocar/imagens`,{
            method: 'POST',
            body: formdata
        }).then(dados => dados.json());
        var form = new FormData();
        form.append('image',imagem.imagens[0][Object.keys(imagem.imagens[0])[0]]);
        form.append('type',type)
        form.append('order',ordem)
        let img = await fetch(`http://127.0.0.1:8000/api/vehicles_save_img/${vehicle_id}`, {
            method: 'POST',
            body: form
        }).then(dados => dados.json())
        return img;
    } else {
        return false;
    }
}

export async function Vehicle_delete_img(company_id,board,type) {
    return await fetch(`http://girocar.leadforce.com.br/ws/girocar/imagens/${company_id}/${board}?CHAVE=${type}`,{
        method: 'DELETE'
    }).then(dados => dados.json());
}

export async function Options() {
    return await fetch(`http://127.0.0.1:8000/api/options`).then(dado => dado.json());
}

export async function buscaImgApp(company_id,board,vehicle_id) {
    let fileList = [
        {
          vehicle_id: vehicle_id,
          type: 'ARQUIVO1',
          image: null,
        },
        {
          vehicle_id: vehicle_id,
          type: 'ARQUIVO2',
          image: null,
        },
        {
          vehicle_id: vehicle_id,
          type: 'ARQUIVO3',
          image: null,
        },
        {
          vehicle_id: vehicle_id,
          type: 'ARQUIVO4',
          image: null,
        },
        {
          vehicle_id: vehicle_id,
          type: 'ARQUIVO5',
          image: null,
        },
        {
          vehicle_id: vehicle_id,
          type: 'ARQUIVO6',
          image: null,
        },
        {
          vehicle_id: vehicle_id,
          type: 'ARQUIVO7',
          image: null,
        },
        {
          vehicle_id: vehicle_id,
          type: 'ARQUIVO8',
          image: null,
        },
        {
          vehicle_id: vehicle_id,
          type: 'ARQUIVO9',
          image: null,
        },
        {
          vehicle_id: vehicle_id,
          type: 'ARQUIVO10',
          image: null,
        },
        {
          vehicle_id: vehicle_id,
          type: 'ARQUIVO11',
          image: null,
        },
        {
          vehicle_id: vehicle_id,
          type: 'ARQUIVO12',
          image: null,
        },
        {
          vehicle_id: vehicle_id,
          type: 'ARQUIVO13',
          image: null,
        },
        {
          vehicle_id: vehicle_id,
          type: 'ARQUIVO14',
          image: null,
        },
        {
          vehicle_id: vehicle_id,
          type: 'ARQUIVO15',
          image: null,
        },
        {
          vehicle_id: vehicle_id,
          type: 'ARQUIVO16',
          image: null,
        },
        {
          vehicle_id: vehicle_id,
          type: 'ARQUIVO17',
          image: null,
        },
        {
          vehicle_id: vehicle_id,
          type: 'ARQUIVO98',
          image: null,
        },
        {
          vehicle_id: vehicle_id,
          type: 'ARQUIVO99',
          image: null,
        }
      ]
      let imgs_app = await fetch(`http://girocar.leadforce.com.br/ws/girocar/imagens/${ company_id }/${ board }`).then(dados => dados.json());
      
      fileList.map( (files, index) => {
        if (imgs_app.veiculos.length > 0) { 
          imgs_app.veiculos[0].imagens.map( img => {
              if ( img.media_number === files.type ) {
                fileList[index] = {type: img.media_number,image: img.url,vehicle_id: vehicle_id,uid: img.id}
              }
              return true;
          })
        }
        return true;
      });
      return fileList;
}

export async function save_img(vehicle_id,imagem,type,ordem,board,typedb) {
    var form = new FormData();
    form.append('image',imagem);
    form.append('type',type)
    form.append('board',board)
    form.append('order',ordem)
    form.append('typedb',typedb)
    let img = await fetch(`http://127.0.0.1:8000/api/vehicles_save_img/${vehicle_id}`, {
        method: 'POST',
        body: form
    }).then(dados => dados.json());
    return img;
  }

export function mask (value) {
    value = value.replace(/\D/g, "");
    value = value.replace(/(\d)(\d{2})$/, "$1,$2");
    value = value.replace(/(?=(\d{3})+(\D))\B/g, ".");
    return value;
}

export function insertVersion(vehicle_model_id,final_year,name) {
    let formData = new FormData();
    formData.append('vehicle_model_id',vehicle_model_id);
    formData.append('final_year',final_year)
    formData.append('name',name)
    fetch(`http://127.0.0.1:8000/api/insert/versions`,{
        method: 'POST',
        body: formData
    })
}

export function insertModel(name,vehicle_brand_id) {
    let formData = new FormData();
    formData.append('name',name);
    formData.append('vehicle_brand_id',vehicle_brand_id)

    fetch(`http://127.0.0.1:8000/api/insert/models`,{
        method: 'POST',
        body: formData
    })
}

export async function insertVehicles(vehicle) {
    var form_data = new FormData();

    for ( var key in vehicle ) {
        form_data.append(key, vehicle[key]);
    }
    return fetch(`http://127.0.0.1:8000/api/insert/vehicles`,{
        method: 'POST',
        body: form_data
    }).then( dados => dados.json());
}