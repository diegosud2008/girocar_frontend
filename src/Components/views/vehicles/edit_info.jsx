
import React, { useEffect, useState } from 'react';
import { Form, Input,Col,Select, Button } from 'antd';
import { vehicles_brands,vehicles_models,vehicle_versions,Vehicles_fuels,Vehicles_colors,Vehicles_segments,Vehicles_exchanges,Vehicle_save,mask } from '../../functions/vehicles';


const layout = {
    labelCol: { span: 8 },
    wrapperCol: { span: 24 },
  };
export const EditInfos = ({vehicle,onClick,atualizaVehicle}) => {
    let [ brands,setBrands ] = useState([]);
    let [ models, setModels ] = useState([]);
    let [ years, setYears ] = useState([]);
    let [ versions, setVersions ] = useState([]);
    let [ fuels, setFuels ] = useState([]);
    let [ colors, setColors ] = useState([]);
    let [ segments, setSegments ] = useState([]);
    let [ exchanges, setExchanges ] = useState([]);
    let [ selectModel, setSelectModel] = useState(vehicle.vehicle_model_id || "");
    let [ selectYealM, setSelectYM] = useState(vehicle.year_model|| 2000);
    let [ price, setPrice ] = useState();
    useEffect(() => {
        async function getItems() {
            try {
                setBrands(await vehicles_brands(vehicle.vehicle_category_id || 1))
                setModels(await vehicles_models(vehicle.vehicle_brand_id || 0))
                setVersions(await vehicle_versions(vehicle.vehicle_model_id,vehicle.year_model))
                setFuels(await Vehicles_fuels());
                setColors(await Vehicles_colors());
                setSegments(await Vehicles_segments());
                setExchanges(await Vehicles_exchanges());
                let array_ = [];
                for (let x = 1970; x <= 2021; x++) {
                    array_.push(x)
                }
                setYears(array_)
            } catch (error) {
              alert("Ocorreu um erro ao buscar os items");
            }
          }
          getItems();
    },[vehicle.vehicle_category_id,vehicle.vehicle_brand_id,vehicle.vehicle_model_id,vehicle.year_model])
    const onFinish = (values) => {
        values = {...values, price: values.price.replace(/\./g,'').replace(',','.'), price_minimum: values.price_minimum && values.price_minimum.replace(/\./g,'').replace(',','.')}
        Vehicle_save(values)
        atualizaVehicle({
            ...vehicle,
            ...values
        })
        onClick();
    };

    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };
    const onChange = async (Objeto) => {
        if (Objeto.vehicle_category_id) {
            setBrands(await vehicles_brands(Objeto.vehicle_category_id));
        }
        if (Objeto.vehicle_brand_id) {
            setModels(await vehicles_models(Objeto.vehicle_brand_id));
        }
        if (Objeto.vehicle_model_id) {
            setSelectModel(Objeto.vehicle_model_id);
        }
        if (Objeto.vehicle_model_id) {
            setSelectModel(Objeto.vehicle_model_id);
            setVersions(await vehicle_versions(Objeto.vehicle_model_id || vehicle.vehicle_model_id,selectYealM || vehicle.year_model))
        }
        if (Objeto.year_model) {
            setSelectYM(Objeto.year_model);
            setVersions(await vehicle_versions(selectModel || vehicle.vehicle_model_id,Objeto.year_model || vehicle.year_model))
        }     
    }

    const onChange_price = (value) => {
        setPrice(mask(value.target.value));
    }
    return(
        <Form
            {...layout}
            name="basic"
            layout="vertical"
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            className="mt-1 d-flex flex-wrap"
            key={vehicle.id}
            >
            <Form.Item
                name="id"
                className="w-100"
                colon={24}
                initialValue={vehicle.id}
                labelCol={24}
                hidden={true}
            >
                <Input/>
            </Form.Item> 
            <Col span={12}>
                <Form.Item
                    label="Placa"
                    name="board"
                    className="w-100"
                    colon={24}
                    initialValue={vehicle.board}
                    labelCol={24}
                    rules={[{ required: true, message: 'Por favor insira a Placa' }]}
                >
                    <Input/>
                </Form.Item>
                <Form.Item
                    label="Categoria"
                    name="vehicle_category_id"
                    className="w-100"
                    colon={24}
                    labelCol={24}
                    initialValue={vehicle.vehicle_category_id}
                    rules={[{ required: true, message: 'Por favor escolha a categoria' }]}
                >
                    <Select placeholder="Selecione" onChange={(value) => onChange({vehicle_category_id: value})}>
                        <Select.Option value={1}>Carro</Select.Option>
                        <Select.Option value={2}>Moto</Select.Option>
                    </Select>
                </Form.Item>
                <Form.Item
                    label="Modelo"
                    name="vehicle_model_id"
                    className="w-100"
                    colon={24}
                    initialValue={vehicle.vehicle_model_id}
                    labelCol={24}
                    rules={[{ required: true, message: 'Por favor escolha um modelo' }]}
                >
                    <Select onChange={(value) => onChange({vehicle_model_id: value})} placeholder="Selecione">
                        {
                            models.map( model => {
                                return <Select.Option value={model.id}>{model.name}</Select.Option>
                            })
                        }
                    </Select>
                </Form.Item>
                <Form.Item
                    label="Versão"
                    name="vehicle_version_id"
                    className="w-100"
                    colon={24}
                    labelCol={24}
                    initialValue={vehicle.vehicle_version_id}
                    rules={[{ required: true, message: 'Selecione uma versão' }]}
                >
                    <Select onChange={(value) => onChange({vehicle_version_id: value})} placeholder="Selecione">
                        { 
                            versions.map( version => {
                                return <Select.Option value={version.id}>{version.name}</Select.Option>
                            })
                        }
                    </Select>
                </Form.Item>
                <Form.Item
                    label="Combustivel"
                    name="vehicle_fuel_id"
                    className="w-100"
                    colon={24}
                    labelCol={24}
                    initialValue={vehicle.vehicle_fuel_id}
                    rules={[{ required: true, message: 'Por favor escolha um combustivel' }]}
                >
                    <Select placeholder="Selecione">
                        {
                            fuels.map( fuel => {
                                return <Select.Option value={fuel.id}>{fuel.name}</Select.Option>
                            })
                        }
                    </Select>
                </Form.Item>
                <Form.Item
                    label="Cor"
                    name="vehicle_color_id"
                    className="w-100"
                    colon={24}
                    labelCol={24}
                    initialValue={vehicle.vehicle_color_id}
                    rules={[{ required: true, message: 'Por favor insira uma cor' }]}
                >
                    <Select placeholder="Selecione">
                        {
                            colors.map( color => {
                                return <Select.Option value={color.id}>{color.name}</Select.Option>
                            })
                        }
                    </Select>
                </Form.Item>
                <Form.Item
                    label="Preço"
                    name="price"
                    className="w-100"
                    colon={24}
                    valuePropName={price}
                    initialValue={vehicle.price ? mask(vehicle.price) : 0}
                    labelCol={24}
                    rules={[{ required: true, message: 'Por favor insira um valor' }]}
                >
                    <Input onChange={(value) => onChange_price(value)} value={price}/>
                </Form.Item>
                <Form.Item
                    label="Chassi"
                    name="chassi"
                    className="w-100"
                    colon={24}
                    labelCol={24}
                    initialValue={vehicle.chassi}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    label="Status"
                    name="status"
                    className="w-100"
                    colon={24}
                    labelCol={24}
                    initialValue={vehicle.status}
                >
                    <Select placeholder="Selecione">
                        <Select.Option value={0}>Ativo</Select.Option>
                        <Select.Option value={1}>Inativo</Select.Option>
                    </Select>
                </Form.Item>
                <Form.Item
                    label="Licenciado"
                    name="licensed"
                    className="w-100"
                    colon={24}
                    labelCol={24}
                    initialValue={vehicle.licensed}
                >
                    <Select placeholder="Selecione">
                        <Select.Option value={0}>Não</Select.Option>
                        <Select.Option value={1}>Sim</Select.Option>
                    </Select>
                </Form.Item>
                <Form.Item
                    label="Adaptado Deficientes Fisicos"
                    name="adapted_people_with_disabilities"
                    className="w-100"
                    colon={24}
                    labelCol={24}
                    initialValue={vehicle.adapted_people_with_disabilities}
                >
                    <Select placeholder="Selecione">
                        <Select.Option value={0}>Não</Select.Option>
                        <Select.Option value={1}>Sim</Select.Option>
                    </Select>
                </Form.Item>
                <Form.Item
                    label="Blindado"
                    name="armored"
                    className="w-100"
                    colon={24}
                    labelCol={24}
                    initialValue={vehicle.armored}
                >
                    <Select placeholder="Selecione">
                        <Select.Option value={0}>Não</Select.Option>
                        <Select.Option value={1}>Sim</Select.Option>
                    </Select>
                </Form.Item>
                <Form.Item
                    label="Alienado"
                    name="alienated"
                    className="w-100"
                    colon={24}
                    labelCol={24}
                    initialValue={vehicle.alienated}
                >
                    <Select placeholder="Selecione">
                        <Select.Option value={0}>Não</Select.Option>
                        <Select.Option value={1}>Sim</Select.Option>
                    </Select>
                </Form.Item>
                <Form.Item
                    label="Revisado Oficina Agenda Do Carro"
                    name="revised_car_agenda_workshop"
                    className="w-100"
                    colon={24}
                    labelCol={24}
                    initialValue={vehicle.revised_car_agenda_workshop}
                >
                    <Select placeholder="Selecione">
                        <Select.Option value={0}>Não</Select.Option>
                        <Select.Option value={1}>Sim</Select.Option>
                    </Select>
                </Form.Item>
                <Form.Item
                    label="Aceito troca"
                    name="swap_accepted"
                    className="w-100"
                    colon={24}
                    labelCol={24}
                    initialValue={vehicle.swap_accepted}
                >
                    <Select placeholder="Selecione">
                        <Select.Option value={0}>Não</Select.Option>
                        <Select.Option value={1}>Sim</Select.Option>
                    </Select>
                </Form.Item>
                <Form.Item
                    label="Approved Plus (Audi)"
                    name="approved_plus"
                    className="w-100"
                    colon={24}
                    labelCol={24}
                    initialValue={vehicle.approved_plus}
                >
                    <Select placeholder="Selecione">
                        <Select.Option value={0}>Não</Select.Option>
                        <Select.Option value={1}>Sim</Select.Option>
                    </Select>
                </Form.Item>
                <Form.Item
                    label="Veículo de coleção"
                    name="collector_vehicle"
                    className="w-100"
                    colon={24}
                    labelCol={24}
                    initialValue={vehicle.collector_vehicle}
                >
                    <Select placeholder="Selecione">
                        <Select.Option value={0}>Não</Select.Option>
                        <Select.Option value={1}>Sim</Select.Option>
                    </Select>
                </Form.Item>
                <Form.Item
                    label="Repasse"
                    name="repasse"
                    className="w-100"
                    colon={24}
                    labelCol={24}
                    initialValue={vehicle.repasse}
                >
                    <Select placeholder="Selecione">
                        <Select.Option value={0}>Não</Select.Option>
                        <Select.Option value={1}>Sim</Select.Option>
                    </Select>
                </Form.Item>
            </Col>



            <Col span={12}>
                <Form.Item
                    label="Nome"
                    name="name"
                    rules={[{ required: true, message: 'Por favor insira o nome' }]}
                    className="w-100"
                    colon={24}
                    
                    initialValue={vehicle.name}
                    labelCol={24}
                >
                    <Input/>
                </Form.Item>
                <Form.Item
                    label="Marca"
                    name="vehicle_brand_id"
                    className="w-100"
                    colon={24}
                    initialValue={vehicle.vehicle_brand_id}
                    labelCol={24}
                    rules={[{ required: true, message: 'Por favor insira a marca' }]}
                >
                    <Select showSearch onChange={(value) => onChange({vehicle_brand_id: value})} placeholder="Selecione" filterOption={(input, option) => option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0}>
                        {
                            brands.map( (brand,index) => {
                                return <Select.Option key={index} value={brand.id}>{brand.name}</Select.Option>
                            })
                        }
                    </Select>
                </Form.Item>
                <Form.Item
                    label="Ano Modelo"
                    name="year_model"
                    className="w-100"
                    colon={24}
                    labelCol={24}
                    initialValue={vehicle.year_model}
                    rules={[{ required: true, message: 'Selecione um ano' }]}
                >
                    <Select onChange={(value) => onChange({year_model: value})} placeholder="Selecione">
                        { 
                            years.map( (x,index) => {
                                return <Select.Option key={index} value={x}>{x}</Select.Option>
                            })
                        }
                    </Select>
                </Form.Item>
                <Form.Item
                    label="Ano Fabricação"
                    name="year_manufacture"
                    className="w-100"
                    colon={24}
                    labelCol={24}
                    initialValue={vehicle.year_manufacture}
                    rules={[{ required: true, message: 'Por favor escolha um ano' }]}
                >
                    <Select placeholder="Selecione">
                        { 
                            years.map( (x,index) => {
                                return <Select.Option key={index} value={x}>{x}</Select.Option>
                            })
                        }
                    </Select>
                </Form.Item>
                <Form.Item
                    label="Qunatidade Portas"
                    name="doors"
                    className="w-100"
                    colon={24}
                    labelCol={24}
                    initialValue={vehicle.doors}
                    rules={[{ required: true, message: 'Por favor escolha uma porta' }]}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    label="Câmbio"
                    name="vehicle_exchange_id"
                    className="w-100"
                    colon={24}
                    labelCol={24}
                    initialValue={vehicle.vehicle_exchange_id}
                    rules={[{ required: true, message: 'Por favor escolha um Câmbio' }]}
                >
                    <Select placeholder="Selecione">
                        {
                            exchanges.map( (exchange,index) => {
                                return <Select.Option key={index} value={exchange.id}>{exchange.name}</Select.Option>
                            })
                        }
                    </Select>
                </Form.Item>
                <Form.Item
                    label="Seguimento"
                    name="vehicle_segment_id"
                    className="w-100"
                    colon={24}
                    labelCol={24}
                    initialValue={vehicle.vehicle_segment_id}
                    rules={[{ required: true, message: 'Por favor escolha um seguimento' }]}
                >
                    <Select placeholder="Selecione">
                        {
                            segments.map( (segment,index) => {
                                return <Select.Option key={index} value={segment.id}>{segment.name}</Select.Option>
                            })
                        }
                    </Select>
                </Form.Item>
                <Form.Item
                    label="Preço Mínimo"
                    name="price_minimum"
                    className="w-100"
                    colon={24}
                    labelCol={24}
                    initialValue={vehicle.price_minimum ? parseFloat(vehicle.price_minimum).toLocaleString('pt-br',{ minimumFractionDigits: 2, maximumFractionDigits: 2 }) : 0}
                    rules={[{ required: true, message: 'Por favor insira um valor' }]}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    label="Quilometragem"
                    name="km"
                    className="w-100"
                    colon={24}
                    labelCol={24}
                    initialValue={vehicle.km}
                    rules={[{ required: true, message: 'Por favor insira a Quilometragem' }]}
                >
                    <Input />
                </Form.Item>
                <Form.Item
                    label="Tipo"
                    name="type"
                    className="w-100"
                    colon={24}
                    labelCol={24}
                    initialValue={vehicle.type}
                >
                    <Select placeholder="Selecione">
                        <Select.Option value="N">Novo</Select.Option>
                        <Select.Option value="U">Usado</Select.Option>
                    </Select>
                </Form.Item>
                <Form.Item
                    label="Garantia de Fabrica"
                    name="factory_warranty"
                    className="w-100"
                    colon={24}
                    labelCol={24}
                    initialValue={vehicle.factory_warranty}
                >
                    <Select placeholder="Selecione">
                        <Select.Option value={0}>Não</Select.Option>
                        <Select.Option value={1}>Sim</Select.Option>
                    </Select>
                </Form.Item>
                <Form.Item
                    label="Único Dono"
                    name="only_owner"
                    className="w-100"
                    colon={24}
                    labelCol={24}
                    initialValue={vehicle.only_owner}
                >
                    <Select placeholder="Selecione">
                        <Select.Option value={0}>Não</Select.Option>
                        <Select.Option value={1}>Sim</Select.Option>
                    </Select>
                </Form.Item>
                <Form.Item
                    label="IPVA Pago"
                    name="ipva_paid"
                    className="w-100"
                    colon={24}
                    labelCol={24}
                    initialValue={vehicle.ipva_paid}
                >
                    <Select placeholder="Selecione">
                        <Select.Option value={0}>Não</Select.Option>
                        <Select.Option value={1}>Sim</Select.Option>
                    </Select>
                </Form.Item>
                <Form.Item
                    label="Revisões Em Concessionária"
                    name="car_dealer_reviews"
                    className="w-100"
                    colon={24}
                    labelCol={24}
                    initialValue={vehicle.car_dealer_reviews}
                >
                    <Select placeholder="Selecione">
                        <Select.Option value={0}>Não</Select.Option>
                        <Select.Option value={1}>Sim</Select.Option>
                    </Select>
                </Form.Item>
                <Form.Item
                    label="Financiado"
                    name="financed"
                    className="w-100"
                    colon={24}
                    labelCol={24}
                    initialValue={vehicle.financed}
                >
                    <Select placeholder="Selecione">
                        <Select.Option value={0}>Não</Select.Option>
                        <Select.Option value={1}>Sim</Select.Option>
                    </Select>
                </Form.Item>
                <Form.Item
                    label="BPS (BMW)"
                    name="BPS"
                    className="w-100"
                    colon={24}
                    labelCol={24}
                    initialValue={vehicle.BPS}
                >
                    <Select placeholder="Selecione">
                        <Select.Option value={0}>Não</Select.Option>
                        <Select.Option value={1}>Sim</Select.Option>
                    </Select>
                </Form.Item>
                <Form.Item
                    label="Destaque"
                    name="destaque"
                    className="w-100"
                    colon={24}
                    labelCol={24}
                    initialValue={vehicle.destaque}
                >
                    <Select placeholder="Selecione">
                        <Select.Option value={0}>Não</Select.Option>
                        <Select.Option value={1}>Sim</Select.Option>
                    </Select>
                </Form.Item>
                <Form.Item
                    label="Vídeo do youtube"
                    name="video_youtube"
                    className="w-100"
                    colon={24}
                    labelCol={24}
                    initialValue={vehicle.video_youtube}
                >
                    <Input/>
                </Form.Item>
            </Col>
            <Col span={24}>
                <Form.Item
                    label="Descríção"
                    name="description"
                    className="w-100"
                    colon={24}
                    labelCol={24}
                    initialValue={vehicle.description}
                >
                    <Input.TextArea rows={4}/>
                </Form.Item>
            </Col>
            <Col span={24}>
            <Form.Item>
                <Button type="primary" htmlType="submit">
                        salvar
                </Button>
            </Form.Item>
            </Col>
        </Form>
    )
}