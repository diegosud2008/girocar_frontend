
export async function save_user(paramers = {}) {
    var formdata = new FormData();
    formdata.append("LOGIN", paramers.username);
    formdata.append("SENHA", paramers.password);
    formdata.append("ACCESS_KEY", "");
    
    let user = await fetch(`https://girocar.leadforce.com.br/ws/girocar/usuarios`, {
        method: 'POST',
        body: formdata
    }).then(dado => dado.json())
    .catch(function(err) {
        console.log(err);
        return false;
    });
    
    let usuer_validation = user.usuario ? true : false;
    switch (usuer_validation) {
        case true:
            if (paramers.remember) {
                localStorage.setItem('user',JSON.stringify({...user.usuario}))
                localStorage.setItem('companies',String(JSON.stringify({...user.empresas})))
                localStorage.setItem('setCompany',user.empresas[0].token);
            } else {
                sessionStorage.setItem('user',JSON.stringify({...user.usuario}))
                sessionStorage.setItem('companies',String(JSON.stringify({...user.empresas})))
                sessionStorage.setItem('setCompany',user.empresas[0].token);
            }
            return true
        default: 
            return false;
    }
}
export function check_use() {
    let logget = sessionStorage.user || localStorage.user ? true :false;
    return logget;
}