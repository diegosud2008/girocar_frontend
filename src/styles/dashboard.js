import styled from 'styled-components';

const Wrapper = styled.div`
.ant-card-head-title {
    text-align: center;
    font-size: 29px;
    padding: 0;
}
.ant-card-body {
    padding-top: 5px;
}
.b-default, .b-default2 {
    .ant-card-head-title {
        color: #fff;
    }
    hr {
        height: 57px;
    }
}
.portais {
    display: flex;
    justify-content: space-around;
    flex-wrap: wrap;

    .btn {
        width: 46%;
        font-size: 15px;
        margin-top: 10px;
    }
    .b-orange {
        background: orange;
    }
    .b-red {
        background: red;
    }
}
.col-portal {
    .card-custom {
        height: 305px;
    }
}
`

export default Wrapper;