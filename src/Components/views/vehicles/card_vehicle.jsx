
import React, { Component } from 'react';
import Style from '../../../styles/card_vehicle';
import { mdiSquareEditOutline,mdiCurrencyUsd,mdiDeleteOutline } from '@mdi/js';
import Icon from '@mdi/react';
import { Edit } from './editModal';

export default class card_vehicule extends Component {
    state = {
        edit: false,
        vihicle_id: "",
        vehicle: this.props
    }
    handleOk = () => {
        this.setState({
            edit: !this.state.edit
        })
    }
    handleCancel = (vehicleAtua) => {
        let vehicle = vehicleAtua;
        this.setState({
            edit: !this.state.edit,
            vehicle
        })
    }
    
    render() {
        let { vehicle } = this.state;
        return(
            <Style>
                <div className="w-100 d-flex pt-1">
                    <span className="img d-block">
                        <img src={vehicle.image && vehicle.image !== 'null' ? vehicle.image : "https://bhi.com.br/wp-content/themes/arkahost52/assets/images/default.jpg"} alt="" className="w-100"/>
                    </span>
                    <div className="infos p-1">
                        <span className="title">
                            <strong>{vehicle.brand} </strong>
                            {vehicle.version}
                        </span>
                        <hr/>
                        <span className="info d-flex flex-wrap">
                            <span className="mr-1">
                                <strong>Placa</strong>
                                <p className="m-0">{vehicle.board}</p>
                            </span>
                            {
                            vehicle.year_model &&
                                <span className="mr-2">
                                    <strong>Ano</strong>
                                    <p className="m-0">{vehicle.year_model}/{vehicle.year_manufacture}</p>
                                </span>
                            }
                            {
                            vehicle.km &&
                                <span className="mr-2">
                                    <strong>KM</strong>
                                    <p className="m-0">{vehicle.km}</p>
                                </span>
                            }
                            <span className="mr-2">
                                <strong>Cor</strong>
                                <p className="m-0">Vermelho</p>
                            </span>
                            {
                            vehicle.price &&
                                <span className="mr-2">
                                    <strong>Preço</strong>
                                    <p className="m-0">R$ {vehicle.price}</p>
                                </span>
                            }
                            <span className="mr-2">
                                <strong>Cadastrado em</strong>
                                <p className="m-0">{`${new Date(vehicle.created_at).getDate()}/${new Date(vehicle.created_at).getMonth()+1}/${new Date(vehicle.created_at).getFullYear()}`}</p>
                            </span>
                            {
                            vehicle.model &&
                                <span className="mr-2">
                                    <strong>Categoria</strong>
                                    <p className="m-0">{vehicle.model}</p>
                                </span>
                            }
                            <span className="mr-2">
                                <strong>Qualidade do anúncio</strong>
                                <p className="m-0">95%</p>
                            </span>
                        </span>
                    </div>
                    <span className="shares">
                        <span>
                            <span className="d-flex align-items-center" onClick={() => this.setState({edit: !this.state.edit})}>
                                <Icon path={mdiSquareEditOutline} size={1} color="#3a3a3a"/>
                                <strong>Editar</strong>
                            </span>
                            <span className="d-flex align-items-center c-orange">
                                <Icon path={mdiCurrencyUsd} size={1} color="orange"/>
                                <strong>Vender</strong>
                            </span>
                            <span className="d-flex align-items-center c-red">
                                <Icon path={mdiDeleteOutline} size={1} color="red"/>
                                <strong>Excluir</strong>
                            </span>
                        </span>
                    </span>
                </div>
                <Edit {...this.state} vehicle={vehicle} handleOk={this.handleOk} handleCancel={this.handleCancel}/>
            </Style>
        )
    }
}